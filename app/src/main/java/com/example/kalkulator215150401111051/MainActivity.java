package com.example.kalkulator215150401111051;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class MainActivity extends AppCompatActivity {

    private TextView layar, tampilanTeks;
    private String tampilan="", hasil ="", operatorTerakhir="";
    private EditText masukan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton delete = (ImageButton) findViewById(R.id.butdelet);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletenumber();
            }
        }
    );
        layar = (TextView)findViewById(R.id.input_box);
        layar.setText(tampilan);
        masukan = findViewById(R.id.input_box);
        tampilanTeks = findViewById(R.id.result_box);
    }
    private void tambahAkhir(String str) {
        this.masukan.getText().append(str);
    }
    public void onClickNumber(View v) {
        Button b = (Button) v;
        tampilan = tampilan + b.getText();
        tambahAkhir(tampilan);
        tampilan="";
    }
    public void onClickOperator(View v) {
        Button b = (Button) v;
        tampilan = tampilan + b.getText();
        if(endsWithOperatore())
        {
            replace(tampilan);
            operatorTerakhir = b.getText().toString();
            tampilan = "";
        }
        else {
            tambahAkhir(tampilan);
            operatorTerakhir = b.getText().toString();
            tampilan = "";
        }
    }
    public void onClearButton(View v) {
        masukan.getText().clear();
        tampilanTeks.setText("");
    }
    public void deletenumber() {
        this.masukan.getText().delete(getinput().length() - 1, getinput().length());
    }
    private String getinput() {
        return this.masukan.getText().toString();
    }
    private boolean endsWithOperatore() {
        return getinput().endsWith("+") || getinput().endsWith("-") || getinput().endsWith("/") || getinput().endsWith("x");
    }
    private void replace(String a) {
        masukan.getText().replace(getinput().length() - 1, getinput().length(), a);
    }
    private double operate(String a,String b,String operasi)
    {
        switch(operasi) {
            case "+": return Double.valueOf(a) + Double.valueOf(b);
            case "-": return Double.valueOf(a) - Double.valueOf(b);
            case "x": return Double.valueOf(a) * Double.valueOf(b);
            case "\u00F7": return Double.valueOf(a) / Double.valueOf(b);
            default: return -1;
        }
    }
    public void equalresult(View v) {
        String masukan = getinput();
        if(!endsWithOperatore()) {
            if (masukan.contains("x")) {
                masukan = masukan.replaceAll("x", "*");
            }
            if (masukan.contains("\u00F7")) {
                masukan = masukan.replaceAll("\u00F7", "/");
            }
            Expression expression = new ExpressionBuilder(masukan).build();
            double result = expression.evaluate();
            tampilanTeks.setText(String.valueOf((int) result));
        }
        else tampilanTeks.setText("");
        System.out.println(hasil);
    }
}